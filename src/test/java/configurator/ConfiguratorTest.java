package configurator;

import modelo.SuscriptionServer;
import notification_system.NotificationReceptor;
import org.junit.Before;
import org.junit.Test;
import utilities.ConfiguratorObserver;
import utils.Configuration;

import static org.junit.Assert.assertTrue;

public class ConfiguratorTest {
    protected SuscriptionServer server = SuscriptionServer.getInstance(new NotificationReceptor());
    protected ConfiguratorObserver observer = new ConfiguratorObserver();

    @Before
    public void configureTest() {
        server.deleteObservers();
        server.addObserver(observer);
    }

    @Test
    public void testTrue() {
        assertTrue(true);
    }

    @Test
    public void testDisabledConsole() {
        server.registerSubscriber("BARBIJOS", "Roberto", Configuration.NOTIFY_MODE_CONSOLE);
        server.changeServiceMode(false, "BARBIJOS", "Roberto", Configuration.NOTIFY_MODE_CONSOLE);
        assertTrue(observer.getLastChange().getClient() == "Roberto" && observer.getLastChange().getMedicament() == "BARBIJOS");
    }

    @Test
    public void testUnknow() {
        server.registerSubscriber("ALCOHOL", "Mario", "ERROR");
        server.changeServiceMode(false, "ALCOHOL", "MARIO", "ERROR");
        assertTrue(observer.getLastChange() == null);
    }

    @Test
    public void testTelegramEnabledDisabled() {
        server.registerSubscriber("ASPIRINAS", "Maria", Configuration.NOTIFY_MODE_TELEGRAM);
        server.changeServiceMode(false, "ASPIRINAS", "Maria", Configuration.NOTIFY_MODE_TELEGRAM);
        server.changeServiceMode(true, "ASPIRINAS", "Maria", Configuration.NOTIFY_MODE_TELEGRAM);
        assertTrue(observer.getLastChange().getClient() == "Maria" && observer.getLastChange().getMode() == Configuration.NOTIFY_MODE_TELEGRAM);
    }

}
