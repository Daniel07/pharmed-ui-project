package notificators;

import interfaces.INotification;
import interfaces.Notifier;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

public class NotifierTelegram extends PharmedBot implements interfaces.NotifierTelegram {

    public NotifierTelegram() {
        System.out.println("NOMBRE DE CLASE: " + getClass().getName());
    }

    @Override
    public void notify(INotification notification) {
        System.out.println("NOTIFICACIóN: " + notification.getMessage());
        //id hardcodeado... deberia venir desde el cuerpo de la notificacion
        sendMessage(notification.getMessage(), Integer.valueOf(notification.getClient()));
    }
}
