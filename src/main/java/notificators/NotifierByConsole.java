package notificators;

import interfaces.INotification;
import interfaces.Notifier;
import interfaces.NotifierConsole;

public class NotifierByConsole implements NotifierConsole {

    public NotifierByConsole() {
        System.out.println("NOMBRE DE CLASE: " + getClass().getName());
    }

    @Override
    public void notify(INotification notification) {
        System.out.println("NOTIFICACIóN: " + notification.getMessage());
    }
}
