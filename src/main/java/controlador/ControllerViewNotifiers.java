package controlador;

import interfaces.Notifier;
import modelo.RemedySubscriber;
import modelo.Stock;
import modelo.SuscriptionServer;
import notification_system.NotificationReceptor;
import utilities.NotifierDTO;
import utils.Configuration;
import vista.ViewNotifiers;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class ControllerViewNotifiers implements Observer, ActionListener, ListSelectionListener {

	private ViewNotifiers view;
	private Stock model;
	public static final String ACTION_GUARDAR = "Guardar";
	public static final String OPTION_ACTIVE = "Activo";
	public static final String OPTION_INACTIVE = "Inactivo";

	protected SuscriptionServer server;


	public ControllerViewNotifiers(ViewNotifiers view, List<Notifier> notifiers, SuscriptionServer server) {
		super();
		this.view = view;
		view.setController(this);
		fillNotifiers(notifiers);
		this.server = server;
	}

	private void fillNotifiers(List<Notifier> lista) {
		DefaultListModel<NotifierDTO> listModel = new DefaultListModel<NotifierDTO>();
		for (Notifier notifier : lista) {
			String name = notifier.getClass().getName();
			name = name.replace("Notifier", "");
			name = name.replace("notificators.", "");
			name = name.replace("By", "");
			listModel.addElement(new NotifierDTO(name));
		}
		view.getListRemediesView().setModel(listModel);
	}

	public ControllerViewNotifiers(Stock model) {
		super();
		this.model = model;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		
		if(event.getActionCommand().equals(ACTION_GUARDAR)) {
			NotifierDTO notifier = (NotifierDTO) view.getListRemediesView().getSelectedValue();
			String option = view.getOpciones().getSelectedItem().toString();
			boolean status = option == OPTION_ACTIVE;
			String mode =  new String();
			if (notifier.getName().toLowerCase().contains("telegram")) {
				mode = Configuration.NOTIFY_MODE_TELEGRAM;
			}
			if (notifier.getName().toLowerCase().contains("console")) {
				mode = Configuration.NOTIFY_MODE_CONSOLE;
			}
			changeStatus(view.getTxtInput().getText(), view.getTxtName().getText(), mode, status);
			//System.out.println("ACTION PERFORMED");
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {

	}

	private void changeStatus(String client, String medicament, String modo, boolean status) {
		server.changeServiceMode(status, medicament, client, modo);
	}
	
	@Override
	public void update(Observable observable, Object object) {

	}
}
