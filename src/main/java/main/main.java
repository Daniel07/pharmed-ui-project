package main;

import controlador.ControllerViewNotifiers;
import interfaces.Notifier;
import modelo.SuscriptionServer;
import notification_system.NotificationReceptor;
import notificators.NotifierByConsole;
import notificators.NotifierTelegram;
import notificators.PharmedBot;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import utilities.ChangesObserver;
import vista.ViewNotifiers;

import java.util.ArrayList;
import java.util.List;

public class main {

	public static void main(String[] args) {
		// Se inicializa el contexto de la API
		ApiContextInitializer.init();

		// Se crea un nuevo Bot API
		final TelegramBotsApi telegramBotsApi = new TelegramBotsApi();

		try {
			// Se registra el bot
			telegramBotsApi.registerBot(new PharmedBot());
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
		//seteo el nombre del package en el core

		// Listado de notificadores
		ViewNotifiers view3 = new ViewNotifiers();
		List<Notifier> notifiers = discoverNotifiers();


		SuscriptionServer server = SuscriptionServer.getInstance(new NotificationReceptor());
		ControllerViewNotifiers controller3 = new ControllerViewNotifiers(view3, notifiers, server);
		ChangesObserver changes = new ChangesObserver();
		server.addObserver(changes);
		view3.show();
	}

	private static List<Notifier> discoverNotifiers() {
		// Buscar los notifiers con el FileFinder del sistema de notif
		// Momentaneamente se fabrican

		ArrayList<Notifier> notifiersList = new ArrayList<>();
		notifiersList.add(new NotifierByConsole());
		notifiersList.add(new NotifierTelegram());
		return notifiersList;
	}

}
