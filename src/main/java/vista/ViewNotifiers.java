package vista;

import controlador.ControllerViewNotifiers;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;

public class ViewNotifiers extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtTotal, txtName, txtInput;
	private JButton btnVender;
	@SuppressWarnings("rawtypes")
	private JList listRemediesView;

	private JComboBox opciones;

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("rawtypes")
	public ViewNotifiers() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 60, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		listRemediesView = new JList();
		listRemediesView.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		GridBagConstraints gbc_listRemediesView = new GridBagConstraints();
		gbc_listRemediesView.gridheight = 6;
		gbc_listRemediesView.insets = new Insets(0, 0, 5, 5);
		gbc_listRemediesView.fill = GridBagConstraints.BOTH;
		gbc_listRemediesView.gridx = 1;
		gbc_listRemediesView.gridy = 2;
		contentPane.add(listRemediesView, gbc_listRemediesView);
		
		JLabel lblName = new JLabel("Cliente :");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 3;
		gbc_lblName.gridy = 2;
		contentPane.add(lblName, gbc_lblName);
		
		txtName = new JTextField();
		txtName.setEnabled(true);
		txtName.setEditable(true);
		GridBagConstraints gbc_txtName = new GridBagConstraints();
		gbc_txtName.insets = new Insets(0, 0, 5, 5);
		gbc_txtName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtName.gridx = 3;
		gbc_txtName.gridy = 3;
		contentPane.add(txtName, gbc_txtName);
		txtName.setColumns(10);
		
		JLabel lblTotal = new JLabel("Medicamento:");
		GridBagConstraints gbc_lblTotal = new GridBagConstraints();
		gbc_lblTotal.insets = new Insets(0, 0, 5, 5);
		gbc_lblTotal.gridx = 3;
		gbc_lblTotal.gridy = 4;
		contentPane.add(lblTotal, gbc_lblTotal);
		
		txtTotal = new JTextField();
		txtTotal.setEnabled(true);
		txtTotal.setEditable(true);
		GridBagConstraints gbc_txtTotal = new GridBagConstraints();
		gbc_txtTotal.insets = new Insets(0, 0, 5, 5);
		gbc_txtTotal.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtTotal.gridx = 3;
		gbc_txtTotal.gridy = 5;
		contentPane.add(txtTotal, gbc_txtTotal);
		txtTotal.setColumns(10);

		// Creacion del JComboBox y añadir los items.
		opciones = new JComboBox();
		opciones.addItem("Activo");
		opciones.addItem("Inactivo");
		GridBagConstraints gbc_opciones = new GridBagConstraints();
		gbc_opciones.insets = new Insets(0, 0, 5, 5);
		gbc_opciones.fill = GridBagConstraints.HORIZONTAL;
		gbc_opciones.gridx = 3;
		gbc_opciones.gridy = 6;
		contentPane.add(opciones, gbc_opciones);
		
		txtInput = new JTextField();
		GridBagConstraints gbc_txtInput = new GridBagConstraints();
		gbc_txtInput.insets = new Insets(0, 0, 5, 5);
		gbc_txtInput.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtInput.gridx = 3;
		gbc_txtInput.gridy = 6;
		//contentPane.add(txtInput, gbc_txtInput);
		txtInput.setColumns(10);
		
		btnVender = new JButton("Guardar");
		GridBagConstraints gbc_btnVender = new GridBagConstraints();
		gbc_btnVender.fill = GridBagConstraints.BOTH;
		gbc_btnVender.insets = new Insets(0, 0, 0, 5);
		gbc_btnVender.gridx = 3;
		gbc_btnVender.gridy = 7;
		contentPane.add(btnVender, gbc_btnVender);


	}

	public void setController(ControllerViewNotifiers controller) {
		btnVender.addActionListener(controller);
		listRemediesView.addListSelectionListener(controller);
	}

	public JTextField getTxtTotal() {
		return txtTotal;
	}

	public void setTxtTotal(JTextField txtTotal) {
		this.txtTotal = txtTotal;
	}

	public JTextField getTxtName() {
		return txtName;
	}

	public void setTxtName(JTextField txtName) {
		this.txtName = txtName;
	}

	public JTextField getTxtInput() {
		return txtInput;
	}

	public void setTxtInput(JTextField txtInput) {
		this.txtInput = txtInput;
	}

	public JButton getBtnVender() {
		return btnVender;
	}

	public void setBtnVender(JButton btnVender) {
		this.btnVender = btnVender;
	}

	@SuppressWarnings("rawtypes")
	public JList getListRemediesView() {
		return listRemediesView;
	}

	@SuppressWarnings("rawtypes")
	public void setListRemediesView(JList listRemediesView) {
		this.listRemediesView = listRemediesView;
	}

	public JComboBox getOpciones() {
		return opciones;
	}

	public void setOpciones(JComboBox opciones) {
		this.opciones = opciones;
	}
}
