package utilities;

public class Utilities {

	public static boolean validateString(String string) {
		return string != null && !string.isEmpty();
	}
}
