package utilities;

import modelo.RemedySubscriber;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ChangesObserver implements Observer {

    @Override
    public void update(Observable observable, Object o) {
        RemedySubscriber suscriber = (RemedySubscriber) o;
        JOptionPane.showMessageDialog(null, "Se guardó la configuración para el cliente " + suscriber.getClient() + " y medicamento " + suscriber.getMedicament() + ". " + (!suscriber.isServiceActived() ? " NO s" : " S" +"e utilizará el servicio: " + suscriber.getMode()));
    }


}
