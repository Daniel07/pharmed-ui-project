package utilities;

import modelo.RemedySubscriber;

public class RemedySuscriberDTO {
    private String medicament;
    private String client;
    private String mode;
    private boolean serviceActived;

    public RemedySuscriberDTO(RemedySubscriber subscriber) {
        medicament = subscriber.getMedicament();
        client = subscriber.getClient();
        mode = subscriber.getMode();
        serviceActived = subscriber.isServiceActived();
    }

    public String getMedicament() {
        return medicament;
    }

    public void setMedicament(String medicament) {
        this.medicament = medicament;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public boolean isServiceActived() {
        return serviceActived;
    }

    public void setServiceActived(boolean serviceActived) {
        this.serviceActived = serviceActived;
    }
}
