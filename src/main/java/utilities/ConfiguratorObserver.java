package utilities;

import modelo.RemedySubscriber;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ConfiguratorObserver implements Observer {
    private List<RemedySuscriberDTO> changes = new ArrayList<RemedySuscriberDTO>();

    @Override
    public void update(Observable observable, Object o) {
        RemedySubscriber suscriber = (RemedySubscriber) o;
        changes.add(new RemedySuscriberDTO(suscriber));
    }

    public RemedySuscriberDTO getLastChange () {
        return changes.size() > 0 ? changes.get(changes.size() -1) : null;
    }

    public void cleanChanges() {
        changes.clear();
    }
}
